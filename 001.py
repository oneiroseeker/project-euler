"""
Project Euler Problem #1
=========================

If we list all the natural numbers below 10 that are multiples of 3 or 5,
we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""

# Get number that we are testing on from user
test_number = 1000
x, y = 3, 5

# Creates a list for adding our multiples of 'x'
multiples = list()

# Add x and y to list by default
multiples.append(x)
multiples.append(y)
# Create function that does math
def math(test):
    while True:
        for z in range(1, test_number):
            num = z * test
            if num <= test_number and num != test:
                multiples.append(num)
            elif num >= test_number:
                break
        break


# Run the function on both 3 and 5
math(x)
math(y)
multiples.sort()

# Test if last item in list is equal to the test number, if so remove it.
if multiples[-1] == test_number:
    multiples.pop()

# Get rid of duplicates in list
multiples = list(set(multiples))

# Get the sum of the multiples and print to screen
print multiples
my_sum = sum(multiples)
print my_sum

